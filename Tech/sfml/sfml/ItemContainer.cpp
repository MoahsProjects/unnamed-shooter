#include "ItemContainer.h"
#include "Global.h"


ItemContainer::ItemContainer(int x, int y) {
	size.x = x;
	size.y = y;
}

void ItemContainer::SetPosition(int x, int y) {
	position.x = x;
	position.y = y;
}



void ItemContainer::Update(double elapsed) {
	if (!_ordened) Order();

	// Mouse Input
	for (size_t i = 0; i < _amountOfItems; i++) {
		if (items[i]->ContainsPoint(Global::mousePosition->x, Global::mousePosition->y)) {
			items[i]->highlight();
			Group.MoveToTop(items[i]->GetIcon());
			//int index = Group.GetIndexOf( items[i]->GetIcon() );
			//if ( index != -1 ) Group.Remove( items[i]->GetIcon() );
			
		}
		else  {
			items[i]->dehighlight();
		}
	}
}


/* Why not Add Item? */
void ItemContainer::SetItem( Item * item ) {
	_amountOfItems++;
	items[_amountOfItems - 1] = item;
	Group.Add( item->GetIcon()  );
	_ordened = false;
}

void ItemContainer::Order() {
	int xOffset = 0;
	int yOffset = 0;
	int itemSize = 64;
	int h_items = size.x / itemSize;
	int v_items = size.y / itemSize;
	for (size_t i = 0; i < _amountOfItems; i++) {

		yOffset = int(i / h_items) * itemSize;
		xOffset = int(i % h_items) * itemSize;

		items[i]->SetPosition(position.x + xOffset, position.y + yOffset);

	}
	_ordened = true;
}

void ItemContainer::Render(sf::RenderWindow * canvas) {
	canvas->draw( Group );
	for (size_t i = 0; i < _amountOfItems; i++) {

		//items[i]->Render(canvas);
		//void* kas = items[i]->GetIcon();
		//sf::RectangleShape per = (sf::RectangleShape) *kas;


		//sf::RectangleShape * p = items[i]->GetIcon(); // pointer to object
		//void * pv = p;          // pointer to void
		//sf::RectangleShape * p2 = static_cast<sf::RectangleShape *>(pv);


		//sf::Drawable * p = items[i]->GetIcon(); // pointer to object
		//sf::RectangleShape * p2 = static_cast<sf::RectangleShape *>(p);


		//canvas->draw(*p2);
		//canvas->draw(items[i]->GetIcon());
		//canvas->draw((sf::RectangleShape) * kas);
	}

}




ItemContainer::~ItemContainer() {


}
