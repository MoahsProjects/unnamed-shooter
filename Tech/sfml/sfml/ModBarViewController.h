#pragma once
#include "ModBarView.h" 
#include "ModView.h" 
#include "boost/signals2.hpp"
typedef boost::signals2::connection Connection;

class ModBarViewController {
public:

	ModBarView * view;
	boost::signals2::signal<void(ModView *, ModBarViewController *)> modMouseDown;

	ModBarViewController( ModBarView * view ); ~ModBarViewController();

	void Update(double elapsed);
	void onModViewAdded( ModView * modView);
	void onMouseDownMod( ModView * mod);
	void onMouseUpMod( ModView * mod);

};

