
#pragma once
#include <SFML/Graphics.hpp>
#include "Item.h"
#include "ItemContainer.h"



class Game {
public:
	const int SCREENWIDTH = 1920;
	const int SCREENHEIGHT = 1080;
	long kas = 0;
	const int particles = 500;
	sf::CircleShape shapes[500];
	sf::CircleShape superBall;

	ItemContainer * container;


	Game();

	void Update(double elapsed);
	void Render(sf::RenderWindow * canvas);

	~Game();

};

