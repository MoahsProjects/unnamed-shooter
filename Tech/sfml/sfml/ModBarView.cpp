#include "ModBarView.h"


ModBarView::ModBarView(  ) {

}

void ModBarView::AddMod(ModView * mod) {
	int pos = getFreePosition();
	if (pos == -1) {
		throw new CustomError::ArrayLimitReachedException("Kas");
	}
	else {
		modViews[pos] = mod;
		addModView(mod);
	}
}

void ModBarView::Render(sf::RenderWindow * canvas) {
	int max = 10;
	for (int i = 0; i < max; i++) {
		if (modViews[i] != nullptr) {
			canvas->draw( * modViews[i]);
		}
	}
}

void ModBarView::Update(double elapsed) {
	int max = 10;
	for (int i = 0; i < max; i++) {
		if (modViews[i] != nullptr) {
			modViews[i]->Update(elapsed);
		}
	}
}


int ModBarView::getFreePosition() {
	int max = 10;
	for (int i = 0; i < max; i++) {
		if (modViews[i] == nullptr) {
			return i;
		}
	}
	return -1;
}



ModBarView::~ModBarView() {
}
