#include "ModView.h"


ModView::ModView(ModView * toClone) {
	model = toClone->model;
	icon.setSize(toClone->icon.getSize());
	offset.x = toClone->offset.x;
	offset.y = toClone->offset.y;
	icon.setOrigin( offset );
	icon.setFillColor( toClone->icon.getFillColor() );
	Add(&icon);
}

ModView::ModView(ModModel * model) {
	this->model = model;
	
	icon.setSize(sf::Vector2f(64.f, 64.f));
	offset.x = 32.f;
	offset.y = 32.f;
	icon.setOrigin(offset);
	icon.setFillColor(sf::Color(rand() % 255, rand() % 255, rand() % 255, 255));

	Add( &icon );
}

void ModView::Update( double elapsed ) {

	if ( ContainsPoint(Global::mousePosition->x, Global::mousePosition->y) ) {

		if (prevMouseState != sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
				mouseDown( this );
			}
			else {
				mouseUp( this );
			}
		}

		prevMouseState = sf::Mouse::isButtonPressed(sf::Mouse::Left);
	}
}

void ModView::SetPosition(sf::Vector2i position) {
	icon.setPosition(position.x, position.y);
}

bool ModView::ContainsPoint(int x, int y) {
	sf::Vector2f pos = icon.getPosition();
	if (pos.x > x + offset.x) return false;
	if (pos.y > y + offset.y) return false;

	sf::Vector2f size = icon.getSize();
	if (pos.x + size.x < x + offset.x) return false;
	if (pos.y + size.y < y + offset.y) return false;

	return true;
}


ModView::~ModView() {

}
