#pragma once
#include <string>
#include <stdexcept>


namespace CustomError {

	class ChildNotFoundException : public std::runtime_error {
	public:
		ChildNotFoundException() :runtime_error("Child not found") {}
		ChildNotFoundException(std::string msg) :runtime_error(msg.c_str()) {}
	};

	class ChildDoubleEntreeException : public std::runtime_error {
	public:
		ChildDoubleEntreeException() :runtime_error("Object is allready a child of the parent") {}
		ChildDoubleEntreeException(std::string msg) :runtime_error(msg.c_str()) {}
	};

	class ArrayLimitReachedException : public std::runtime_error {
	public:
		ArrayLimitReachedException() :runtime_error("Array is full") {}
		ArrayLimitReachedException(std::string msg) :runtime_error(msg.c_str()) {}
	};

	

};