#include "WeaponEditorController.h"


WeaponEditorController::WeaponEditorController( WeaponEditorView * view ) {
	this->view = view;
	view->modbarViewAdd.connect(boost::bind(&WeaponEditorController::onBarViewAdded, this, _1));
}


void WeaponEditorController::onBarViewAdded( ModBarView * added ) {
	ModBarViewController * controller = new ModBarViewController(added);
	controller->modMouseDown.connect(boost::bind(&WeaponEditorController::onStartDrag, this, _1, _2));

}

void WeaponEditorController::Update(double elapsed) {
	if (dragging) {
		if (prevMouseState != sf::Mouse::isButtonPressed(sf::Mouse::Left) && sf::Mouse::isButtonPressed(sf::Mouse::Left) == false ) {
			onStopDrag();
		}
		else {
			currentlyDragging->SetPosition(*Global::mousePosition);
		}
		prevMouseState = sf::Mouse::isButtonPressed(sf::Mouse::Left);
	}

}

void WeaponEditorController::onStartDrag(ModView * mod, ModBarViewController * bar) {
	
	currentlyDragging = new ModView( mod );
	view->currentlyDragging = currentlyDragging;

	originalDragging = mod;
	dragParent = bar;
	dragging = true;
	
}

void WeaponEditorController::onStopDrag() {

	delete currentlyDragging;
	currentlyDragging = nullptr;
	view->currentlyDragging = nullptr;

	originalDragging = nullptr;
	dragParent = nullptr;
	dragging = false;

}

WeaponEditorController::~WeaponEditorController() {
}
