#define _SCL_SECURE_NO_WARNINGS
#pragma once

#include "WeaponEditor.h"
#include "ModBar.h"
#include "ModBarView.h"
#include "DrawableGroup.h"
#include <SFML/Graphics.hpp>
#include "boost/signals2.hpp"
#include <iostream>
//typedef boost::signals2::signal< void(ModBar*) >
typedef boost::signals2::connection Connection;

class WeaponEditorView {
public:

	ModBarView * inventoryBar;
	ModBarView * launchModsBar;
	ModView * currentlyDragging;

	
	boost::signals2::signal< void(ModBarView *) > modbarViewAdd;

	WeaponEditorView(  );
	~WeaponEditorView();

	void init();
	void loadMods();
	void Render( sf::RenderWindow * canvas );
	void Update( double elapsed );

	

private:
	DrawableGroup children;

};

