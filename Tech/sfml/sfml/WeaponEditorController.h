#pragma once
#include "WeaponEditor.h"
#include "WeaponEditorView.h"
#include "ModBarViewController.h"
class WeaponEditorController {
public:

	bool prevMouseState = false;
	bool dragging = false;
	ModView * currentlyDragging;
	ModView * originalDragging;
	ModBarViewController * dragParent;
	WeaponEditorView * view;

	WeaponEditorController(  WeaponEditorView * view);
	~WeaponEditorController();

	void Update(double elapsed);
	void onBarViewAdded(ModBarView * added);
	void WeaponEditorController::onStartDrag(ModView * mod, ModBarViewController * bar);
	void WeaponEditorController::onStopDrag();
};

