#define _SCL_SECURE_NO_WARNINGS
#pragma once
#include "DrawableGroup.h"
#include "ModModel.h"
#include "CustomError.h"

#include "boost/signals2.hpp"
#include "Global.h"
#include "ModView.h"

typedef boost::signals2::connection Connection;

class ModView : public DrawableGroup {
	
public:

	ModModel * model;
	bool prevMouseState = false;
	boost::signals2::signal<void(ModView * )> mouseDown;
	boost::signals2::signal<void(ModView * )> mouseUp;

	ModView(ModView * toClone);
	ModView(ModModel * model);
	~ModView();

	void Update();
	bool ContainsPoint(int x, int y);
	void Update(double elapsed);
	void SetPosition(sf::Vector2i position);

	


private:
	sf::RectangleShape icon;
	sf::Vector2f offset;


};

