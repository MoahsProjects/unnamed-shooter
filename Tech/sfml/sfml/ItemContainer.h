#pragma once
#include "Item.h"
#include "DrawableGroup.h"

class ItemContainer {
public:
	ItemContainer(int x, int y);

	sf::Vector2i position;
	sf::Vector2i size;


	Item *items[10];
	DrawableGroup Group;

	void Update(double elapsed);
	void Render(sf::RenderWindow * canvas);

	void Order();
	void SetItem(Item * item);
	void SetSize();
	void SetPosition(int x, int y);


	~ItemContainer();


private:
	bool _ordened;
	int _amountOfItems = 0;

};

