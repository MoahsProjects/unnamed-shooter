#pragma once
#include <SFML/Graphics.hpp>
class Item {
public:
	Item(sf::Color color);
	~Item();

	void SetPosition(int x, int y);
	void Render(sf::RenderWindow * canvas);
	void Update(double elapsed);
	bool ContainsPoint(int x, int y);

	bool highlighted = false;
	void highlight();
	void dehighlight();
	sf::RectangleShape * GetIcon();
	sf::Vector2f offset;

private:
	sf::RectangleShape icon;

};

