#define _SCL_SECURE_NO_WARNINGS
#include "WeaponEditorView.h"



WeaponEditorView::WeaponEditorView() {
	currentlyDragging = nullptr;
}

void WeaponEditorView::init() {
	inventoryBar = new ModBarView();
	inventoryBar->position.x = 0;
	inventoryBar->position.y = 200;
	inventoryBar->size.x = 64*10;
	inventoryBar->size.y = 64;
	modbarViewAdd(inventoryBar);

	launchModsBar = new ModBarView();
	launchModsBar ->position.x = 32;
	launchModsBar ->position.y = 32;
	launchModsBar ->size.x = 64 * 10;
	launchModsBar ->size.y = 64;
	modbarViewAdd( launchModsBar );

}

void WeaponEditorView::loadMods() {

	ModModel * mod = new ModModel();
	inventoryBar->AddMod( new ModView( mod ) );

	mod = new ModModel();
	launchModsBar->AddMod(new ModView(mod));
}


void WeaponEditorView::Render( sf::RenderWindow * canvas ) {
	inventoryBar->Render(canvas);
	launchModsBar->Render(canvas);
	if (currentlyDragging != nullptr) canvas->draw( * currentlyDragging );

}

void WeaponEditorView::Update(double elapsed) {
	inventoryBar->Update(elapsed);
	launchModsBar->Update(elapsed);
}

WeaponEditorView::~WeaponEditorView() {


}
