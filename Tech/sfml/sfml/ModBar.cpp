#include "ModBar.h"


ModBar::ModBar() {

}


void ModBar::AddMod(  ModModel * mod ) {

	int pos = getFreePosition();
	if (pos == -1) {
		throw new CustomError::ArrayLimitReachedException("Kas");
	}
	else {
		mods[pos] = mod;
		modAdded(mod);
	}

}

int ModBar::getFreePosition() {
	int max = 5;
	for (int i = 0; i < max; i++) {
		if (mods[i] == nullptr) {
			return i;
		}
	}
	return -1;
}

ModBar::~ModBar() {
}
