#pragma once
#include <SFML/Graphics.hpp>


class DrawableGroup : public sf::Drawable {
public:

	// display list
	sf::Drawable * drawables[ 100 ];

	DrawableGroup();
	~DrawableGroup();
	
	void Add(sf::Drawable * child);
	void Add(sf::Drawable * child, int index);


	/* Removes the object from the display list */
	void Remove(sf::Drawable * child);
	int GetIndexOf( sf::Drawable * child );
	void MoveToTop(sf::Drawable * child);
	void MoveToBack(sf::Drawable * child);
	int GetTopIndex();	
	
private:
	int position;
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	void GaurdDoubleEntry(sf::Drawable * child);

};

