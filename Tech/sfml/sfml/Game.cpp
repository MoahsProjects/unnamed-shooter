#include "Game.h"
#include "DrawableGroup.h"

#include <iostream>
using namespace std;



Game::Game() {
	for (size_t i = 0; i < particles; i++) {
		sf::CircleShape shape;
		shape.setRadius(4 + rand() % 20 + 0.f);
		shape.setPosition(rand() % SCREENWIDTH + 0.f, rand() % SCREENHEIGHT + 0.f);
		shape.setFillColor(sf::Color(rand() % 255, rand() % 255, rand() % 255, 128 + rand() % 128));
		shapes[i] = shape;
	}

	container = new ItemContainer(64 * 5, 500);
	container->SetPosition(SCREENWIDTH - container->size.x, SCREENHEIGHT / 2);
	for (size_t i = 0; i < 10; i++) {
		container->SetItem(new Item(sf::Color(rand() % 255, rand() % 255, rand() % 255, 255)));

	}


	superBall.setRadius(30);
	superBall.setPosition(SCREENWIDTH - 75, rand() % SCREENHEIGHT + 0.f);
	superBall.setFillColor(sf::Color(rand() % 255, rand() % 255, rand() % 255, 128 + rand() % 128));
	
}


void Game::Update(double elapsed) {
	for (size_t i = 0; i < particles; i++) {
		sf::Vector2f pos = shapes[i].getPosition();

		double xSpeed = elapsed / 10.f;
		double ySpeed = elapsed / 10.f;

		if ((i % 3) == 2) xSpeed *= -1;
		if ((i % 2) == 1) ySpeed *= -1;

		pos.x = (pos.x + xSpeed);
		pos.y = (pos.y + ySpeed);
		if (pos.x > SCREENWIDTH - 150) {
			pos.x -= (SCREENWIDTH - 150);
		}
		else if (pos.x < 0) {
			pos.x += (SCREENWIDTH - 150);

		}
		if (pos.y > SCREENHEIGHT) {
			pos.y -= SCREENHEIGHT;
		}
		else if (pos.y < 0) {
			pos.y += SCREENHEIGHT;

		}
		shapes[i].setPosition(pos);
	}
	sf::Vector2f pos = superBall.getPosition();
	pos.y += elapsed;
	if (pos.y > SCREENHEIGHT) {
		pos.y -= SCREENHEIGHT;
	}
	superBall.setPosition(pos);

	container->Update(elapsed);

}
void Game::Render(sf::RenderWindow * canvas) {
	for (size_t i = 0; i < particles; i++) {
		canvas->draw(shapes[i]);
	}
	canvas->draw(superBall);
	container->Render(canvas);

	//DrawableGroup test;
	//canvas->draw(test);

}


Game::~Game() {

}
