#include "Item.h"


Item::Item(sf::Color color) {
	icon.setSize(sf::Vector2f(64.f, 64.f));
	offset.x = 32.f;
	offset.y = 32.f;
	icon.setOrigin(offset);
	icon.setFillColor(sf::Color(rand() % 255, rand() % 255, rand() % 255, 255));

}

void Item::Render(sf::RenderWindow * canvas) {
	canvas->draw(icon);
}

void Item::Update(double elapsed) {
	// update position

}

void Item::highlight() {
	highlighted = true;
	icon.setScale( 1.5, 1.5 );
}
void Item::dehighlight() {
	highlighted = false;
	icon.setScale( .8f, .8f );
}

void Item::SetPosition(int x, int y) {
	icon.setPosition(x, y);
}

bool Item::ContainsPoint(int x, int y) {
	sf::Vector2f pos = icon.getPosition();
	if (pos.x > x + offset.x) return false;
	if (pos.y > y + offset.y) return false;

	sf::Vector2f size = icon.getSize();
	if (pos.x + size.x < x + offset.x) return false;
	if (pos.y + size.y < y + offset.y) return false;

	return true;
}

sf::RectangleShape * Item::GetIcon() {
	return &icon;

}

Item::~Item() {


}