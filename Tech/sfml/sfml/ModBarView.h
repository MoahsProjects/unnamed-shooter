#pragma once

#include "Modbar.h"
#include "ModView.h"
#include <SFML/Graphics.hpp>
#include "CustomError.h"

typedef boost::signals2::connection Connection;

class ModBarView {
public:
	ModView *modViews[10] = {};

	sf::Vector2i position;
	sf::Vector2i size;
	boost::signals2::signal< void(ModView*) > addModView;
	
	ModBarView(  ); ~ModBarView();


	int getFreePosition();
	void AddMod(ModView * mod);
	void Render(sf::RenderWindow * canvas);
	void Update(double elapsed);
	
};

