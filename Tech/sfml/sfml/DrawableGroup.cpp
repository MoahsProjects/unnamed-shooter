#include "DrawableGroup.h"
#include <iostream>
#include "CustomError.h"

DrawableGroup::DrawableGroup() {
	position = 0;
}

void DrawableGroup::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	for (int i = position - 1; i >= 0; i--) {
		sf::RectangleShape * rectPointer = static_cast<sf::RectangleShape *>(drawables[i]);
		target.draw(*rectPointer, states);
	}
}

/* Child will be added to the top of the DisplayList */
void DrawableGroup::Add( sf::Drawable * child ) {
	GaurdDoubleEntry( child );
	drawables[ position ] = child;
	position++;
}

/* Child will be added at specified index */
void DrawableGroup::Add(sf::Drawable * child, int index) {
	GaurdDoubleEntry(child);
	sf::Drawable * previous = child;
	position++;
	for (int i = index; i < position; i++) {
		sf::Drawable * temp = drawables[i];
		drawables[i] = previous;
		previous = temp;
	}
}

void DrawableGroup::Remove( sf::Drawable * child ) {
	int deleteIndex = GetIndexOf( child );
	if ( deleteIndex == -1 ) throw new CustomError::ChildNotFoundException( "Kas" );

	sf::Drawable * previous = NULL;
	for (int i = position - 1; i >= deleteIndex; i-- ) {
		sf::Drawable * temp = drawables[i];
		drawables[i] = previous;
		previous = temp;
	}
	position--;
}

int DrawableGroup::GetIndexOf( sf::Drawable * child ) {
	for (int i = position - 1; i >= 0; i--) {
		if (drawables[i] == child) return i;
	}
	return -1;
}


/* Move child to the the display.*/
void DrawableGroup::MoveToTop( sf::Drawable * child) {
	int currentIndex = GetIndexOf( child );
	if ( currentIndex == 0 ) return; // allready on the index

	Remove( child );
	Add( child,0 );

}

/* Move child to the back of the display. Will be rendered first, the rest is rendered on top of him.*/
void DrawableGroup::MoveToBack(sf::Drawable * child) {
	int index = GetTopIndex();
	int currentIndex = GetIndexOf(child);
	if (currentIndex == index) return; // allready on the index

	Remove(child);
	Add(child);

}


/* returs the index of the child that is on top of the display*/
int DrawableGroup::GetTopIndex() {
	return position-1;
}


void DrawableGroup::GaurdDoubleEntry(sf::Drawable * child) {
	int index = GetIndexOf(child);
	if (index != -1) throw new CustomError::ChildDoubleEntreeException("Kas");
}

DrawableGroup::~DrawableGroup() {

}
