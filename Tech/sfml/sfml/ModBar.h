#define _SCL_SECURE_NO_WARNINGS
#pragma once
#include "boost/signals2.hpp"
#include "ModModel.h"
#include "CustomError.h"
#include "ModView.h"

typedef boost::signals2::signal<void(ModModel*)> Signal;
typedef boost::signals2::connection Connection;

class ModBar {
public:

	ModModel * mods[5];
	Signal modAdded;

	ModBar();
	void ModBar::AddMod( ModModel * mod );
	int ModBar::getFreePosition();
	~ModBar();

};

