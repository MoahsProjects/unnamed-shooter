#include "ModBarViewController.h"


ModBarViewController::ModBarViewController(ModBarView * view) {
	this->view = view;
	view->addModView.connect(boost::bind(&ModBarViewController::onModViewAdded, this, _1));

}

void ModBarViewController::onModViewAdded( ModView * modView ) {
	//(modView);
	modView->mouseDown.connect(boost::bind(&ModBarViewController::onMouseDownMod, this, _1));
	modView->mouseUp.connect(boost::bind(&ModBarViewController::onMouseUpMod, this, _1));
	modView->SetPosition( view->position );
}

void ModBarViewController::Update(double elapsed) {
	
}


void ModBarViewController::onMouseDownMod(ModView * mod) {
	modMouseDown( mod, this );
}

void ModBarViewController::onMouseUpMod(ModView * mod) {

}

ModBarViewController::~ModBarViewController() {
}
