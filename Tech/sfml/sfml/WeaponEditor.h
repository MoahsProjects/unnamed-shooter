#define _SCL_SECURE_NO_WARNINGS
#pragma once
#include "boost/signals2.hpp"
#include "ModBar.h"
typedef boost::signals2::signal< void( ModBar*) > ModSignal;
typedef boost::signals2::connection Connection;


class WeaponEditor {
public:
	WeaponEditor();
	~WeaponEditor();

	ModBar * barTest;
	ModSignal onDrawableAdd;

	void Update(double elapsed);
	void AddTest();
	void init();
	void loadMods();
};

