#define _SCL_SECURE_NO_WARNINGS
#include <SFML/Graphics.hpp>
#include <iostream>
#include "Game.h"
#include "Global.h"
#include "WeaponEditorController.h"
#include "WeaponEditorView.h"

using namespace std;
sf::Vector2i * Global::mousePosition = new sf::Vector2i(0, 0);


int main() {


	

	Game * game = new Game();
	//WeaponEditor * editor = new WeaponEditor();
	WeaponEditorView * editorView = new WeaponEditorView(  );
	WeaponEditorController * editor = new WeaponEditorController(editorView);
	editorView->init();
	editorView->loadMods();

	sf::RenderWindow * window = new sf::RenderWindow(sf::VideoMode(game->SCREENWIDTH, game->SCREENHEIGHT), "SFML Application");
	sf::Clock clock;



	while (window->isOpen()) {
		if (sf::Keyboard::isKeyPressed( sf::Keyboard::Q )) {
			window->close();
		}

		sf::Event event;
		while (window->pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				window->close();
			}
		}

		//sf::Vector2i pos = sf::Mouse::getPosition() - window->getPosition();
		sf::Vector2i pos = sf::Mouse::getPosition( * window );
		Global::mousePosition->x = pos.x;
		Global::mousePosition->y = pos.y;
		sf::Time elapsed = clock.restart();

		double milisElapsed = elapsed.asMicroseconds() / 1000.f;
		game->Update(milisElapsed);
		editor->Update(milisElapsed);
		editorView->Update(milisElapsed);

		window->clear();
		game->Render(window);
		editorView->Render( window );
		window->display();
	}

	return 0;
}